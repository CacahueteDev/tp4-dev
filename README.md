# I. Simple bs program

## 1. First steps

### Serveur
> Fichier : [bs_server_I1.py](bs_server_I1.py)
```
[rocky@server tp4-dev]$ python bs_server_I1.py
Connected by ('10.1.2.11', 46748)
Données reçues du client : b'yo'
```

### Client
> Fichier : [bs_client_I1.py](bs_client_I1.py)
```
[rocky@client tp4-dev]$ python bs_client_I1.py
Le serveur a répondu b'Hi mate !'
```

### Quitter proprement
```py
# On quitte proprement
sys.exit(0)
```

### Commandes
Commandes du côté serveur :
```shell
[rocky@server ~]$ git clone https://gitlab.com/CacahueteDev/tp4-dev
[...]
[rocky@server ~]$ sudo firewall-cmd --permanent --zone=public --add-port=8888/tcp
success
[rocky@server ~]$ cd tp4-dev/
[rocky@server tp4-dev]$ python bs_server_II2A.py
Connected by ('10.1.2.11', 42468)
Données reçues du client : b'Meooooo !'
```
Le programme python écoute bien à la porte :
```
[rocky@server ~]$ sudo ss -laputen | grep 8888
tcp   LISTEN 0      1               0.0.0.0:8888      0.0.0.0:*     users:(("python",pid=1620,fd=3)) uid:1000 ino:21468 sk:6 cgroup:/user.slice/user-1000.slice/session-3.scope <->
```
![Le programme python écoute à la porte (IA)](res/python_listening.png)

Côté client :
```shell
[rocky@server ~]$ git clone https://gitlab.com/CacahueteDev/tp4-dev
[...]
[rocky@client ~]$ cd tp4-dev/
[rocky@client tp4-dev]$ python bs_client_I3.py
Le serveur a répondu b'Hi mate !'
```

## 2. User friendly

### Client
> Fichier : [bs_client_I2.py](bs_client_I2.py)

Voilà l'exécution :
```shell
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :waf
Le serveur a répondu b'ptdr t ki'
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :meo
Le serveur a répondu b'Meo \xc3\xa0 toi confr\xc3\xa8re.'
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :yo
Le serveur a répondu b'Mes respects humble humain.'
```

### Serveur
> Fichier : [bs_server_I2.py](bs_server_I2.py)

Exécution :
```shell
[rocky@server tp4-dev]$ python bs_server_II2A.py
Un client vient de se co et son IP c'est ('10.1.2.11', 45568)
Données reçues du client : b'waf'
[rocky@server tp4-dev]$ python bs_server_II2A.py
Un client vient de se co et son IP c'est ('10.1.2.11', 54154)
Données reçues du client : b'meo'
[rocky@server tp4-dev]$ python bs_server_II2A.py
Un client vient de se co et son IP c'est ('10.1.2.11', 53444)
Données reçues du client : b'yo'
```

## 3. You say client I hear control

### Client
> Fichier : [bs_client_I3.py](bs_client_I3.py)

```shell
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :yo
Traceback (most recent call last):
  File "/home/rocky/tp4-dev/bs_client_I1.py", line 28, in <module>
    raise Exception('invalid input: should be \'meo\' or \'waf\'')
Exception: invalid input: should be 'meo' or 'waf'
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :waf
Le serveur a répondu b'ptdr t ki'
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :meo
Le serveur a répondu b'Meo \xc3\xa0 toi confr\xc3\xa8re.'
[rocky@client tp4-dev]$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :6
Traceback (most recent call last):
  File "/home/rocky/tp4-dev/bs_client_I3.py", line 28, in <module>
    raise Exception('invalid input: should be \'meo\' or \'waf\'')
Exception: invalid input: should be 'meo' or 'waf'
```

# II. You say dev I say good practices

## 1. Args

### Serveur
> Fichier : [bs_server_II1.py](bs_server_II1.py)

```shell
[rocky@server tp4-dev]$ python bs_server_II2A.py -p 4
ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.
[rocky@server tp4-dev]$ python bs_server_II2A.py -g
usage: bs_server_II2A.py [-h] [-p PORT]
bs_server_II2A.py: error: unrecognized arguments: -g
[rocky@server tp4-dev]$ python bs_server_II2A.py -h
usage: bs_server_II2A.py [-h] [-p PORT]

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT
```

## 2. Logs

### A. Logs serveur
> Fichier : [bs_server_II2A.py](bs_server_II2A.py)

Il faut créer le dossier de logs au préalable :
```shell
[rocky@server tp4-dev]$ sudo mkdir /var/log/bs_server
[rocky@server tp4-dev]$ sudo chown -R rocky:rocky /var/log/bs_server
```

On exécute le serveur :
```shell
[rocky@server tp4-dev]$ python bs_server_II2A.py
2023-11-26 10:44:09,213 INFO Le serveur tourne sur :13337
2023-11-26 10:44:11,559 INFO Un client ('10.1.2.11', 43900) s'est connecté.
2023-11-26 10:44:14,048 INFO Le client ('10.1.2.11', 43900) a envoyé b'meo'
2023-11-26 10:44:14,049 INFO Réponse envoyée au client ('10.1.2.11', 43900) : Meo à toi confrère.
```

Le fichier de logs :
```shell
[rocky@server tp4-dev]$ cat /var/log/bs_server/bs_server.log
2023-11-26 10:44:09,213 INFO Le serveur tourne sur :13337
2023-11-26 10:44:11,559 INFO Un client ('10.1.2.11', 43900) s'est connecté.
2023-11-26 10:44:14,048 INFO Le client ('10.1.2.11', 43900) a envoyé b'meo'
2023-11-26 10:44:14,049 INFO Réponse envoyée au client ('10.1.2.11', 43900) : Meo à toi confrère.
```

### B. Logs client

> Fichier : [bs_client_II2B.py](bs_client_II2B.py)

On exécute le client :

```shell
[rocky@client tp4-dev]$ python bs_client_II2B.py
ERROR Impossible de se connecter au serveur 10.1.2.10 sur le port 13337.
[rocky@client tp4-dev]$ python bs_client_II2B.py
Que veut-tu envoyer au serveur :meo
```

Et on récupère le fichier de log :
```shell
[rocky@client tp4-dev]$ cat /var/log/bs_client/bs_client.log
2023-11-26 11:56:31,262 ERROR Impossible de se connecter au serveur 10.1.2.10 sur le port 13337.
2023-11-26 11:57:02,080 INFO Connexion réussie à 10.1.2.10:13337.
2023-11-26 11:57:06,073 INFO Message envoyé au serveur 10.1.2.10 : meo.
2023-11-26 11:57:06,074 INFO Réponse reçue du serveur 10.1.2.10 : b'Meo \xc3\xa0 toi confr\xc3\xa8re.'.
```

## III. Compute

### Client
> Fichier : [bs_client_III.py](bs_client_III.py)

```shell
[rocky@client tp4-dev]$ python bs_client_III.py
Tapez un calcul :3 + 1
Réponse reçue du serveur 10.1.2.10 : b'4'
```

### Serveur
> Fichier : [bs_server_III.py](bs_server_III.py)

```shell
[rocky@server tp4-dev]$ python bs_server_III.py
2023-11-26 12:37:15,902 INFO Le serveur tourne sur :13337
2023-11-26 12:37:18,076 INFO Un client ('10.1.2.11', 60822) s'est connecté.
2023-11-26 12:37:19,937 INFO Le client ('10.1.2.11', 60822) a envoyé 3+1 de type <class 'str'>
2023-11-26 12:37:19,937 INFO Réponse envoyée au client ('10.1.2.11', 60822) : 4
```

![eval()](https://i.redd.it/90zl3avma9641.jpg)