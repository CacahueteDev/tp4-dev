import socket
import sys
import re

# On définit la destination de la connexion
host = '10.1.2.10'  # IP du serveur
port = 13337  # Port choisir par le serveur

# Création de l'objet socket de type TCP (SOCK_STREAM)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connexion au serveur
try:
    s.connect((host, port))
except Exception as e:
    print(f"La connexion à foiré : {e}")
    exit(1)
# note : la double parenthèse n'est pas une erreur : on envoie un tuple à la fonction connect()

print(f"Connecté avec succès au serveur {host} sur le port {port}")

accepted_regex = '(meo)|(waf)'
serv_input = input('Que veut-tu envoyer au serveur :')

if type(serv_input) != str:
    raise TypeError()
if not re.match(accepted_regex, serv_input):
    raise Exception('invalid input: should be \'meo\' or \'waf\'')

s.sendall(bytes(serv_input, 'utf-8'))

# On reçoit 1024 bytes qui contiennent peut-être une réponse du serveur
data = s.recv(1024)

# On libère le socket TCP
s.close()

# Affichage de la réponse reçue du serveur
print(f"Le serveur a répondu {repr(data)}")

# On quitte proprement
sys.exit(0)
